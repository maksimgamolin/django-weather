from typing import Type, Union
from .units import BaseUnit
from .unit_storage import UnitStorage


class TemperatureEntity(object):

    __doc__ = """Сущность температуры с единицой измерения"""

    __slots__ = 'value', 'unit'

    def __init__(self,
                 value: float,
                 unit: Union[str,Type[BaseUnit]]):
        self.value = value
        if isinstance(unit, str):
            unit_storage = UnitStorage()
            self.unit = unit_storage.get_unit(unit)
        else:
            self.unit = unit

    def __eq__(self, other: 'TemperatureEntity'):
        return self.value == other.convert_to_unit(self.unit).value

    def __gt__(self, other: 'TemperatureEntity'):
        return self.value > other.convert_to_unit(self.unit).value

    def __ge__(self, other: 'TemperatureEntity'):
        return self.value >= other.convert_to_unit(self.unit).value

    def convert_to_unit(self,
                        unit: Union[str, Type[BaseUnit]]) -> 'TemperatureEntity':
        """
        Возвращает новую сущность, с новой единицей измерения из unit,
        можетприниматькак строку, так и полноценный объект единицы измерения,
        отнаследованый от BaseUnit.
        :return:
        Возвращает TemperatureEntity(Значение температуры после ковертации ,
        Новая единица измерения)
        """
        if isinstance(unit, str):
            unit_storage = UnitStorage()
            new_unit = unit_storage.get_unit(unit)
        else:
            new_unit = unit
        pre_converted_val = self.unit.revert(value=self.value)
        converted_val = new_unit.convert(pre_converted_val)
        return TemperatureEntity(converted_val, unit)

