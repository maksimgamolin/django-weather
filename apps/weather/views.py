import datetime
from django.conf import settings
from django.utils import timezone
from rest_framework.generics import ListAPIView
from .models import TemperatureMeasure
from .serializers import TemperatureMeasureSerializer


class TemperatureBaseAPIView(ListAPIView):

    __doc__ = "Базовая вьюха"

    serializer_class = TemperatureMeasureSerializer

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        unit_name = self.request.parser_context['kwargs'].get('unit', None)
        if unit_name is None:
            unit_name = 'CELSIUS'
        unit_name = unit_name.upper()
        if unit_name.upper() not in settings.TEMPERATURE['units']:
            unit_name = 'CELSIUS'
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, unit=unit_name.upper(), **kwargs)


class TemperatureNowAPIView(TemperatureBaseAPIView):

    __doc__ = """Погодные измерения на текущий час
    /temperature/kelvin/now/ где unit - единица измерения"""

    queryset = TemperatureMeasure.objects.now()


class TemperatureTodayAPIView(TemperatureBaseAPIView):

    __doc__ = """Выводит измерения за весь день
    /temperature/kelvin/today/ где unit - единица измерения"""

    queryset = TemperatureMeasure.objects.today()


class TemperatureNearAPIView(TemperatureBaseAPIView):

    __doc__ = """Выводит измененения за несколько дней, по умолчанию 3
    
    /temperature/kelvin/near/10/ или /temperature/kelvin/near/ где period - 10 - 
    количестводней которые добавить unit - единица измерения"""

    def get_queryset(self, *args, **kwargs):
        period = self.request.parser_context['kwargs'].get('period', None)
        if period is not None:
            return TemperatureMeasure.objects.near(add_days=period)
        return TemperatureMeasure.objects.near()


class TemperatureConcreteAPIView(TemperatureBaseAPIView):

    __doc__ = """Возвращает результаты измерения на конкретную дату, пример запроса 
    /temperature/celsius/concrete/2018-10-18-09-22/ где celsius это unit, а 2018-22-17-09-22
    это date в формате ГГГГ-ММ-ДД-ЧЧ-мм 
    """

    def get_queryset(self, *args, **kwargs):
        date_raw = self.request.parser_context['kwargs'].get('date', None)
        try:
            date = datetime.datetime.strptime(date_raw, '%Y-%m-%d-%H-%M')
        except Exception:
            date = timezone.now()
        return TemperatureMeasure.objects.concrete(date)

    def get_serializer(self, *args, **kwargs):
        kwargs['many'] = False
        return super(TemperatureConcreteAPIView, self).get_serializer(*args, **kwargs)