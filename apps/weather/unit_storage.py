from importlib import import_module
from inspect import getmembers, isclass
from django.conf import settings
from .units import BaseUnit


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class UnitStorage(object, metaclass=Singleton):

    __doc__ = 'Хранит в словаре единицы измерения, которые указаны в настройках'

    __slots__ = 'units'

    def __init__(self):
        self.units = {}
        for module_str in settings.TEMPERATURE['modules']:
            module = import_module(module_str)
            for name, obj in getmembers(module):
                if (isclass(obj) and
                        BaseUnit in obj.__bases__ and
                        obj.name in settings.TEMPERATURE['units']):
                    self.units[obj.name] = obj

    def get_unit(self, unit_name):
        unit = self.units.get(unit_name, None)
        if unit is None:
            raise ValueError(
                'Unknown unit, may be you forget add him in settings.TEMPERATURE?'
            )
        return unit()
