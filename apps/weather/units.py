from abc import abstractmethod, ABC
from django.conf import settings


class BaseUnit(ABC):

    __slots__ = ()
    __doc__ = """Сущность единицы измерения"""

    @property
    @abstractmethod
    def name(self):
        """
        Имя должно совпадать с именем в  настройках, например CELSIUS
        """
        pass

    @abstractmethod
    def convert(self, value: float) -> float:
        """
        Конвертирует температуру из установленой
        в настройках по умолчанию ед. измерения в текущую
        :return: result (float)
        """
        pass

    @abstractmethod
    def revert(self, value: float) -> float:
        """
        Конвертирует из текущей ед измерения
        в по умолчанию
        :return: result (float)
        """

    def __eq__(self, other: 'BaseUnit'):
        return self.name == other.name


class CelsiusUnit(BaseUnit):

    __slots__ = ()

    name = 'CELSIUS'

    def convert(self, value: float) -> float:
        return value

    def revert(self, value: float) -> float:
        return value


class FahrenheitUnit(BaseUnit):

    __slots__ = ()

    name = 'FAHRENHEIT'

    def convert(self, value: float) -> float:
        return round((value * 1.8 + 32), 1)

    def revert(self, value: float) -> float:
        return round(((value - 32) / 1.8), 1)


class KelvinUnit(BaseUnit):

    __slots__ = ()

    name = 'KELVIN'

    def convert(self, value: float) -> float:
        return round((value + 273.15), 1)

    def revert(self, value: float) -> float:
        return round((value - 273.15), 1)
