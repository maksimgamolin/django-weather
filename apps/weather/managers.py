import datetime
from django.db.models import Manager
from django.utils import timezone


class TMManager(Manager):

    def today(self):
        """
        Отдает все измерения для текущего дня
        """
        return self.filter(
            date__range=(
                datetime.datetime.combine(timezone.now(), datetime.time.min),
                datetime.datetime.combine(timezone.now(), datetime.time.max)
            )
        )

    def now(self):
        """
        Отдает все измерения для текущего часа, текущей даты
        """
        now_time = timezone.now()
        return self.filter(
            date__range=(
                datetime.datetime.combine(
                    now_time,
                    datetime.time.min.replace(hour=timezone.now().hour)
                ),
                datetime.datetime.combine(
                    now_time,
                    datetime.time.max.replace(hour=timezone.now().hour)
                )
            )
        )

    def near(self, add_days: int=3):
        """
        Отдает измерения от начала текущего дня до конца периода.
        :param add_days:
        целое число - количество дней которое добавляется к текущей дате
        """
        last_date = timezone.now()+datetime.timedelta(days=add_days)
        return self.filter(
            date__range=(
                datetime.datetime.combine(timezone.now(), datetime.time.min),
                datetime.datetime.combine(last_date, datetime.time.max)
            )
        )

    def concrete(self, date):
        return self.filter(date__lte=date).first()
