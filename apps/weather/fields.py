from django.db.models import FloatField
from django.conf import settings
from .entities import TemperatureEntity
from .unit_storage import UnitStorage


class TemperatureField(FloatField):

    description = 'Custom temperature field for model'

    def __init__(self,
                 *args,
                 **kwargs):
        unit_storage = UnitStorage()
        self.unit = unit_storage.get_unit(settings.TEMPERATURE['default'])
        super(TemperatureField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if isinstance(value, TemperatureEntity):
            return value.convert_to_unit(self.unit)
        return TemperatureEntity(value, self.unit)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return TemperatureEntity(value, self.unit)

    def get_db_prep_save(self, value, connection):
        if isinstance(value, TemperatureEntity):
            te = value.convert_to_unit(self.unit)
            return super(TemperatureField,
                         self).get_db_prep_save(te.value, connection)
        return super(TemperatureField,
                     self).get_db_prep_save(value, connection)