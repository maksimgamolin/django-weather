import datetime
from random import randrange
from django.core.management.base import BaseCommand
from apps.weather.models import TemperatureMeasure


class Command(BaseCommand):
    help = 'Наполнение базы данных случайными значениями, это продлится несколько минут'

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        date1 = datetime.datetime.combine(datetime.date(2017, 1, 1), datetime.time.min)
        date2 = datetime.datetime.combine(datetime.date(2019, 12, 31), datetime.time.max)
        hour = datetime.timedelta(hours=1)

        while date1 <= date2:
            print(f'Наполняем базу значениемя за {date1}')
            TemperatureMeasure.objects.create(date=date1, temperature=randrange(-10, 30))
            # Привет июльские морозы и новогодняя жара
            date1 = date1 + hour
