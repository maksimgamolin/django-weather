from django.db import models
from .fields import TemperatureField
from .managers import TMManager


class TemperatureMeasure(models.Model):

    objects = TMManager()

    date = models.DateTimeField()
    temperature = TemperatureField()

    class Meta:
        app_label = 'weather'
        ordering = ['-date']

    def __str__(self):
        return self.date