from datetime import datetime, timedelta, time
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from apps.weather import units, entities, unit_storage, models


class TestUnits(TestCase):

    def test_celsius(self):
        celsius = units.CelsiusUnit()
        self.assertEqual(1, celsius.convert(1))
        self.assertEqual(1, celsius.revert(1))

    def test_fahrenheit(self):
        fahrenheit = units.FahrenheitUnit()
        self.assertEqual(33.8, fahrenheit.convert(1))
        self.assertEqual(1, fahrenheit.revert(33.8))

    def test_kelvin(self):
        kelvin = units.KelvinUnit()
        self.assertEqual(274.1, kelvin.convert(1))
        self.assertEqual(1, kelvin.revert(274.1))


class TestTemperatureEntity(TestCase):

    def setUp(self):
        self.celsius = units.CelsiusUnit()
        self.fahrenheit = units.FahrenheitUnit()

    def test_temperature_entity(self):
        te = entities.TemperatureEntity(1, self.celsius)
        te_fs = entities.TemperatureEntity(1, 'CELSIUS')
        self.assertEqual(1, te.value)
        self.assertEqual(te, te_fs)
        te_f = entities.TemperatureEntity(33.8, self.fahrenheit)
        self.assertEqual(te.convert_to_unit(self.fahrenheit), te_f)
        self.assertEqual(te.convert_to_unit('FAHRENHEIT'), te_f)
        te.value = 2
        self.assertTrue(te > te_f)
        self.assertTrue(te >= te_f)
        self.assertFalse(te < te_f)
        self.assertFalse(te <= te_f)


class TestUnitStorage(TestCase):

    def test_create(self):
        us = unit_storage.UnitStorage()
        another_us = unit_storage.UnitStorage()
        self.assertEqual(us, another_us)


class TestTemperatureMeasure(TestCase):

    def test_create(self):
        self.assertEqual(0, models.TemperatureMeasure.objects.count())
        models.TemperatureMeasure.objects.create(date=datetime.now(),
                                                 temperature=1)
        self.assertEqual(1, models.TemperatureMeasure.objects.count())
        te = entities.TemperatureEntity(1, 'CELSIUS')
        models.TemperatureMeasure.objects.create(date=datetime.now(),
                                                 temperature=te)
        self.assertEqual(2, models.TemperatureMeasure.objects.count())
        tm = models.TemperatureMeasure.objects.last()
        self.assertEqual(te, tm.temperature)
        self.assertEqual(entities.TemperatureEntity(33.8, 'FAHRENHEIT'), tm.temperature)
        te2 = entities.TemperatureEntity(33.8, 'FAHRENHEIT')
        models.TemperatureMeasure.objects.create(date=datetime.now(),
                                                 temperature=te2)
        tm = models.TemperatureMeasure.objects.last()
        self.assertEqual(te2, tm.temperature)


class TestTMManager(TestCase):

    def test_now(self):
        models.TemperatureMeasure.objects.create(date=timezone.now().replace(minute=00, second=00, microsecond=00),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=timezone.now().replace(minute=59),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(hours=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(hours=1),
                                                 temperature=4)
        self.assertEqual(5, models.TemperatureMeasure.objects.count())
        self.assertEqual(3, models.TemperatureMeasure.objects.now().count())

    def test_today(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(days=1),
                                                 temperature=4)
        self.assertEqual(5, models.TemperatureMeasure.objects.count())
        self.assertEqual(3, models.TemperatureMeasure.objects.today().count())

    def test_near(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(days=1),
                                                 temperature=4)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=2),
                                                 temperature=5)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=3),
                                                 temperature=6)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=4),
                                                 temperature=7)
        self.assertEqual(8, models.TemperatureMeasure.objects.count())
        self.assertEqual(6, models.TemperatureMeasure.objects.near().count())
        self.assertEqual(7, models.TemperatureMeasure.objects.near(4).count())

    def test_concrete(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        mc = models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                      temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        self.assertEqual(3, models.TemperatureMeasure.objects.count())
        self.assertEqual(mc, models.TemperatureMeasure.objects.concrete(timezone.now()))


class TestTemperatureNowAPIView(TestCase):

    def test_200_now(self):
        models.TemperatureMeasure.objects.create(date=timezone.now().replace(minute=00, second=00, microsecond=00),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=timezone.now().replace(minute=59),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(hours=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(hours=1),
                                                 temperature=4)
        r = self.client.get(
            reverse('temperature:now', kwargs={'unit': 'CELSIUS'})
        )
        self.assertEqual(r.status_code, 200)

    def test_200_today(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(days=1),
                                                 temperature=4)
        r = self.client.get(
            reverse('temperature:today', kwargs={'unit': 'CELSIUS'})
        )
        self.assertEqual(r.status_code, 200)

    def test_200_near(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=1),
                                                 temperature=3)
        models.TemperatureMeasure.objects.create(date=timezone.now() - timedelta(days=1),
                                                 temperature=4)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=2),
                                                 temperature=5)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=3),
                                                 temperature=6)
        models.TemperatureMeasure.objects.create(date=timezone.now() + timedelta(days=4),
                                                 temperature=7)
        r = self.client.get(
            reverse('temperature:near', kwargs={'unit': 'CELSIUS'})
        )
        self.assertEqual(r.status_code, 200)

        r2 = self.client.get(
            reverse('temperature:near-clarification', kwargs={'unit': 'CELSIUS', 'period': 4})
        )
        self.assertEqual(r2.status_code, 200)

    def test_200_concrete(self):
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.min),
                                                 temperature=0)
        models.TemperatureMeasure.objects.create(date=timezone.now(),
                                                 temperature=1)
        models.TemperatureMeasure.objects.create(date=datetime.combine(timezone.now(), time.max),
                                                 temperature=2)
        raw_date = timezone.now().strftime('%Y-%M-%d-%H-%M')
        r = self.client.get(
            reverse('temperature:concrete', kwargs={'unit': 'CELSIUS', 'date': raw_date})
        )
        self.assertEqual(r.status_code, 200)

