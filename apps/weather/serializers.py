from django.conf import settings
from rest_framework import serializers
from .models import TemperatureMeasure
from .entities import TemperatureEntity
from .unit_storage import UnitStorage


class TemperatureMeasureSerializer(serializers.ModelSerializer):

    temperature = serializers.SerializerMethodField()
    unit = settings.TEMPERATURE['default']

    class Meta:
        model = TemperatureMeasure
        fields = '__all__'

    def __init__(self, *args, unit=None, **kwargs):
        if unit is not None:
            self.unit = unit
        super(TemperatureMeasureSerializer, self).__init__(*args, **kwargs)

    def get_temperature(self, obj: TemperatureEntity):
        if self.unit != settings.TEMPERATURE['default']:
            unit_storage = UnitStorage()
            new_unit = unit_storage.get_unit(self.unit)
            entity = obj.temperature.convert_to_unit(new_unit)
        else:
            entity = obj.temperature
        return {'value': entity.value,
                'unit': entity.unit.name}
