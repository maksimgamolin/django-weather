from django.urls import path, re_path
from .views import TemperatureNowAPIView, TemperatureTodayAPIView, \
    TemperatureNearAPIView, TemperatureConcreteAPIView


urlpatterns = [
    path('now/', TemperatureNowAPIView.as_view(), name='now'),
    path('today/', TemperatureTodayAPIView.as_view(), name='today'),
    re_path(r'^near/$', TemperatureNearAPIView.as_view(), name='near'),
    path('near/<int:period>/',
         TemperatureNearAPIView.as_view(),
         name='near-clarification'),
    re_path(r'^concrete/(?P<date>\d{4}-\d{2}-\d{2}-\d{2}-\d{2})/$',
            TemperatureConcreteAPIView.as_view(),
            name='concrete')
]