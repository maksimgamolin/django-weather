Документация на api доступна по ссылке при запущеном сервере

    http://localhost:8000/docs/

Разворачивается через докер-композ или по старинке. Версия питона не ниже 3.6

Если по старинке 
--
Все зависимости лежат в 

    requirements.txt

нужно не забыть

    python manage.py makemigrations
    python manage.py migrate
    python manage.py rand_fill
   
Последняя комманда наполняет значениями базу

Если докер-композ
--

Все как обычно собственно, 

    docker-compose up

но иногда бывает ошибка

    t/tcp:0.0.0.0:8000:tcp:172.18.0.2:8000: input/output error
    ERROR: Encountered errors while bringing up the project.

Фиксится перезапуском докера


