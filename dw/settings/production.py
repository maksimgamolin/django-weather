from .base import *
DEBUG = False
ALLOWED_HOSTS = ['app.project_name.com', ]
STATICFILES_ROOT = os.path.join(BASE_DIR, "static")