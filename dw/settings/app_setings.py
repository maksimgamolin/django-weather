TEMPERATURE = {
    'units': ('CELSIUS', 'FAHRENHEIT', 'KELVIN'),
    'modules': ('apps.weather.units', ),
    'default': 'CELSIUS',
}