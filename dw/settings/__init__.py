from os import environ

if environ.get('BUILD_TYPE') == 'DEV':
    from .development import *
elif environ.get('BUILD_TYPE') == 'PROD':
    from .production import *
else:
    print('Unknown settings, develop installed')
    from .development import *

from .app_setings import *
